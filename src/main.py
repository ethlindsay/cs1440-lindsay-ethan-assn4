import ImagePainter
import FractalFactory
import GradientFactory
import sys

file = sys.argv[1]
gradientName = ""
if len(sys.argv) > 2:
    gradientName = sys.argv[2]

gradient = GradientFactory.makeGradient(gradientName,file)
fractal = FractalFactory.makeFractal(file)

ImagePainter.displayWindow(fractal, gradient, file)



