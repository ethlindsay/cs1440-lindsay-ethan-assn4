import sys
from tkinter import Tk, Canvas, PhotoImage, mainloop

def displayWindow(fractal, gradient, imagename):
    # Set up the GUI so that we can paint the fractal image on the screen
    # and into a PNG image.
    # Display the image on the screen

    window = Tk()
    image = PhotoImage(width=fractal.pixel, height=fractal.pixel)
    paint(fractal, gradient, image, imagename)

    canvas = Canvas(window, width=fractal.pixel, height=fractal.pixel, bg=gradient[0])
    canvas.pack()
    canvas.create_image((fractal.pixel / 2, fractal.pixel / 2), image=image, state="normal")


    # Save the image as a PNG
    # TODO: I have heard that you can create pictures in other formats, such as GIF
    #       and PPM.  I wonder how I do that?
    image.write(imagename + ".png")
    print(f"Wrote image {imagename}.png")
    mainloop()

def paint(fractal, gradient, image, imagename):
    """Paint a Fractal image into the TKinter PhotoImage canvas.  Assumes the
    image is 640x640 pixels.
    This function displays a really handy status bar so you can see how far
    along in the process the program is."""

    # TODO: I feel bad about all of the global variables I'm using.
    #       There must be a better way...

    # Figure out how the boundaries of the PhotoImage relate to coordinates on
    # the imaginary plane.
    minX = fractal.centerX - (fractal.axisLength / 2.0)
    maxX = fractal.centerX + (fractal.axisLength / 2.0)
    minY = fractal.centerY - (fractal.axisLength / 2.0)
    maxY = fractal.centerY + (fractal.axisLength / 2.0)

    # At this scale, how much length and height on the imaginary plane does one
    # pixel take?
    pixelsize = abs(maxX - minX) / fractal.pixel

    portion = int(fractal.pixel / 64)
    for col in range(fractal.pixel):
        if col % portion == 0:
            # Update the status bar each time we complete 1/64th of the rows
            pips = col // portion
            pct = col / fractal.pixel
            print(f"{imagename} ({fractal.pixel}x{fractal.pixel}) {'=' * pips}{'_' * (64 - pips)} {pct:.0%}",
                  end='\r', file=sys.stderr)
        for row in range(fractal.pixel):
            x = minX + col * pixelsize
            y = minY + row * pixelsize
            color = gradient[fractal.count(complex(x, y))]
            image.put(color, (col, row))
    print(
        f"{imagename} ({fractal.pixel}x{fractal.pixel}) ================================================================ 100%",
        file=sys.stderr)


