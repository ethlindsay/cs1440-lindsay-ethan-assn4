import os

#opens a file from the data directory and returns its contents
def openFile(filename):
    if str(filename).startswith("C:"):
        pass
    else:
        filename = 'data/' + filename
    os.chdir(os.getcwd().replace("src",""))
    with open(filename, "r") as f:
        fractal = f.read()
    return fractal

#creates a python dictionary from a fractal document
def createDictionary(fractal):
    fractalList = (fractal.split())
    fractalDictionary = {}
    for i in range(0,len(fractalList),2):
        fractalDictionary[(str)(fractalList[i].replace(":","")).lower()] = fractalList[i+1]
    return (fractalDictionary)
