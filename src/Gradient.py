from abc import ABC, abstractmethod
import Color

class Gradient(ABC):
    def __init__(self, start, end, steps):
        self.start = start
        self.end = end
        self.steps = steps

    @abstractmethod
    def getGradient(self):
        pass

    @abstractmethod
    def createGradient(self):
        pass

class NormalGradient(Gradient):
    def __init__(self, start, end, steps):
        Gradient.__init__(self, start, end, steps)

    def getGradient(self):
        return self.gradientList

    def createGradient(self):
        gradient1 = []
        gradientHex = []
        red = int(self.start[0])
        green = int(self.start[1])
        blue = int(self.start[2])
        for i in range(self.steps):
            red += ((self.end[0] - self.start[0]) / (self.steps - 1))
            green += ((self.end[1] - self.start[1]) / (self.steps - 1))
            blue += ((self.end[2] - self.start[2]) / (self.steps - 1))
            gradient1.append((red,green,blue))
        for j in gradient1:
            color = Color.Color(round(j[0]), round(j[1]), round(j[2]))
            color.setRed(j[0])
            color.setBlue(j[1])
            color.setGreen(j[2])
            gradientHex.append(color.getHexidecimal())
        self.gradientList = gradientHex
        return gradientHex

class StrangeGradient(Gradient):
    def __init__(self, start, end, steps):
        Gradient.__init__(self, start, end, steps)

    def getGradient(self):
        return self.gradientList

    def createGradient(self):
        gradient1 = []
        gradientHex = []
        for i in range(self.steps):
            if i % 2 == 0:
                red = (self.start[0]+i*((self.end[0] - self.start[0]) / (self.steps - 1)))
                green = (self.start[1]+i*(self.end[1] - self.start[1]) / (self.steps - 1))
                blue = (self.start[2]+i*(self.end[2] - self.start[2]) / (self.steps - 1))
            else:
                red = (self.start[0] - i * ((self.end[0] - self.start[0]) / (self.steps - 1)))
                green = (self.start[1] - i * (self.end[1] - self.start[1]) / (self.steps - 1))
                blue = (self.start[2] - i * (self.end[2] - self.start[2]) / (self.steps - 1))
            gradient1.append((red,green,blue))
        for j in gradient1:
            color = Color.Color(round(j[0]), round(j[1]), round(j[2]))
            color.setRed(j[0])
            color.setBlue(j[1])
            color.setGreen(j[2])
            gradientHex.append(color.getHexidecimal())
        self.gradientList = gradientHex
        return gradientHex
