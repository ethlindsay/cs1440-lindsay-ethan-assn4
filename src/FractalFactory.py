import Config
import Fractal

def makeFractal(file):
    fractalDictionary = Config.createDictionary(Config.openFile(file))
    if str(fractalDictionary['type']).lower() == "julia":
        fractal = Fractal.JuliaFractal(file)
    elif str(fractalDictionary['type']).lower() == "mandelbrot":
        fractal = Fractal.MandlebrotFractal(file)
    else:
        fractal = Fractal.MandlebrotFractalSquared(file)
    return fractal