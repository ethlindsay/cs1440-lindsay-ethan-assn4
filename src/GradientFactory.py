import Config
import Gradient

def makeGradient(gradiantName, file):
    fractalDictionary = Config.createDictionary(Config.openFile(file))
    steps = int(fractalDictionary['iterations'])
    type1 = ''
    start = (250,250,250)
    end = (254,0,0)

    if 'strange' in gradiantName:
        type1 = 'strange'
    if gradiantName == "":
        print("GradientFactory: Creating default color gradient")
    elif 'blue' in gradiantName:
        end = (0,254,0)
        print(f"GradientFactory: Creating {type1}blue color gradient")
    elif 'green' in gradiantName:
        end = (0,0,254)
        print(f"GradientFactory: Creating {type1}green color gradient")
    else:
        print(f"GradientFactory: Unknown gradient. Creating {type1}default color gradient")

    if type1 == 'strange':
        gradient = Gradient.StrangeGradient(start,end, steps)
    else:
        gradient = Gradient.NormalGradient(start, end, steps)
    return gradient.createGradient()
