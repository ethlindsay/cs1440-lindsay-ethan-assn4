from abc import ABC, abstractmethod
import Config

class Fractal(ABC):
    def __init__(self, fracFile):
        self.fracDictionary = Config.createDictionary(Config.openFile(fracFile))
        self.type = self.fracDictionary['type']
        self.pixel = int(self.fracDictionary['pixels'])
        self.centerX = float(self.fracDictionary['centerx'])
        self.centerY = float(self.fracDictionary['centery'])
        self.axisLength = float(self.fracDictionary['axislength'])
        self.iterations = int(self.fracDictionary['iterations'])

    @abstractmethod
    def count(self, coordinates):
        pass


class MandlebrotFractal(Fractal):
    def __init__(self, fracFile):
        Fractal.__init__(self,fracFile)


    def count(self, complexCoordinate):
        """Return the count of the current pixel within the Mandelbrot set"""
        z = complex(0, 0)  # z0

        for i in range(self.iterations):
            z = z * z + complexCoordinate  # Get z1, z2, ...
            if abs(z) > 2:
                return i  # The sequence is unbounded
        return self.iterations - 1  # Indicate a bounded sequence

class JuliaFractal(Fractal):
    def __init__(self, fracFile):
        Fractal.__init__(self,fracFile)
        self.cReal = self.fracDictionary['creal']
        self.cImag = self.fracDictionary['cimag']

    def count(self, complexCoordinate):
        z = complex(-1, 0) # c0

        for i in range(self.iterations):
            complexCoordinate = complexCoordinate * complexCoordinate + z # Get z1, z2, ...
            if abs(complexCoordinate) > 2:
                return i # The sequence is unbounded
        return self.iterations - 1 # Indicate a bounded sequence

class MandlebrotFractalSquared(Fractal):
    def __init__(self, fracFile):
        Fractal.__init__(self,fracFile)

    def count(self, complexCoordinate):
        """Return the count of the current pixel within the Mandelbrot set"""
        z = complex(0, 0)  # z0

        for i in range(self.iterations):
            z = z * (z ** 2) + complexCoordinate  # Get z1, z2, ...
            if abs(z) > 2:
                return i  # The sequence is unbounded
        return self.iterations - 1  # Indicate a bounded sequence

